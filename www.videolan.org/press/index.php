<?php
   $title = "Press center";
   $lang = "en";
   $new_design = true;
   $body_color = "orange";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<div class="container">

<h1>Press Releases from VideoLAN</h1>

<h2><a href="/press/vlc-apple-tv.html">Announcing VLC for Apple TV</a></h2>
<h4>2016-01-12</h4>
<p class="projectDescription">
    The VLC team is proud to announce the first port of VLC on the Apple TV!
    <br />
    VLC on the Apple TV allows you to get access to all your files and video streams in their native formats without conversions, directly on the new Apple device and your TV.
    <br />
    <b><a href="/press/vlc-apple-tv.html">More</a></b>
</p>
<h2><a href="/press/vlc-2.2.0.html">VLC media player 2.2.0 released</a></h2>
<h4>2015-02-27</h4>
<p class="projectDescription">
    VideoLAN and the VLC development teams are proud to publish a major new release of VLC.
    <br />
    The 2.2.0 release of VLC media player for the desktop is coordinated, for the first time ever, with releases on iOS, Android (including Android TV), WinRT (Windows RT and x86) and Windows Phone.<i>VLC runs everywhere!</i>
    <br />
    <b><a href="/press/vlc-2.2.0.html">More</a></b>
</p>
<h2><a href="/press/2014-1.html">2014-1</a></h2>
<h4>2014-04-01</h4>
<p class="projectDescription">
    Press Release about VLC distributed decoding and conecoin
    <br />
    VLC starts a distributed codec and a new currency
    <br />
    <b><a href="/press/2014-1.html">More</a></b>
</p>
<h2><a href="/press/ios2out.html">VLC for iOS 2.0 released</a></h2>
<h4>2013-07-18</h4>
<p class="projectDescription">
    We have exciting news to share. VLC for iOS will be <a href="https://itunes.apple.com/app/id650377962">back on the App Store</a> today!
    <br />
    It will be available free of charge in any country, requires iOS 5.1 or later and runs on any iPhone, iPad or iPod touch.
    <br />
    This is more than an upgrade of the initial version: it's a full re-write. It's way faster thanks to modern output modules for audio and video, offers multi-core decoding, and supports any video files available in VLC media player for desktop operating systems.
    <br />
    <b><a href="/press/ios2out.html">More</a></b>
</p>
<h2><a href="/press/win8ks-ended.html">Successful fundraising campaign for VLC ports to Windows 8, RT and Phone</a></h2>
<h4>2012-12-29</h4>
<p class="projectDescription">
    Today, the <a href="http://www.kickstarter.com/projects/1061646928/vlc-for-the-new-windows-8-user-experience-metro" target="_blank">kickstarter campaign</a> run by several core members of the VLC development team ended. £47,056 was pledged by 3080 generous backers. We would like to thank everyone who made this possible
    <br />
    This is a tremendous and somewhat unexpected success. We are very pleased about it.
    <br />
    <b><a href="/press/win8ks-ended.html">More</a></b>
</p>
<h2><a href="/press/win8ks.html">VLC for the new Windows 8 User Experience fundraiser</a></h2>
<h4>2012-11-29</h4>
<p class="projectDescription">
    Today, some VideoLAN team members launched a fundraiser on <a href="http://www.kickstarter.com/projects/1061646928/vlc-for-the-new-windows-8-user-experience-metro">Kickstarter</a> to support a port to the new User Experience on Windows 8 (aka "Metro") and Windows RT.
    <br />
    <b><a href="/press/win8ks.html">More</a></b>
</p>
<h2><a href="/press/lgpl-modules.html">VLC playback modules relicensed to LGPL</a></h2>
<h4>2012-11-20</h4>
<p class="projectDescription">
    As announced in <a href="/press/lgpl.html">previous</a> <a href="/press/lgpl-libvlc.html">press releases</a>, VideoLAN and VLC developers have completed the process of changing the license of most of the VLC playback modules to <a href="http://www.gnu.org/licenses/lgpl-2.1.html">LGPL</a>. In addition to last year's <a href="/press/lgpl-libvlc.html">relicensing of the VLC engine</a>, this allows the creation of fullly-LGPL playback applications, based on <a href="/vlc/">VLC</a> technology.
    <br />
    <b><a href="/press/lgpl-modules.html">More</a></b>
</p>
<h2><a href="/press/lgpl-libvlc.html">libVLC relicensed to LGPL</a></h2>
<h4>2011-12-21</h4>
<p class="projectDescription">
    As announced in a <a href="/press/lgpl.html">previous press release</a>, VideoLAN and VLC developers have completed the process of changing the license of the VLC engine to <a href="http://www.gnu.org/licenses/lgpl-2.1.html">LGPL</a>. The <a href="http://ecp.fr">École Centrale Paris</a> shares its <a href="http://www.ecp.fr/home/Actualites?actuID=19452">happiness</a> about <a href="http://www.ecp.fr/files/content/sites/ecp_internet/files/Presse/cp_vlc_211211.pdf">this change</a>.
    <br />
    <b><a href="/press/lgpl-libvlc.html">More</a></b>
</p>
<h2><a href="/press/lgpl.html">libVLC and LGPL</a></h2>
<h4>2011-09-07</h4>
<p class="projectDescription">
    During the third <a href="/videolan/events/vdd11.html">VideoLAN Dev Days</a>, last weekend in Paris, numerous developers approved the process of changing the license of the VLC engine to <a href="http://www.gnu.org/licenses/lgpl-2.1.html">LGPL</a>.
    <br />
    <b><a href="/press/lgpl.html">More</a></b>
</p>
<h2><a href="/press/2010-1.html">2010-1</a></h2>
<h4>2010-06-21</h4>
<p class="projectDescription">
    Press Release about Shoutcast Removal in VLC
    <br />
    <b><a href="/press/2010-1.html">More</a></b>
</p>
<h2><a href="/press/patents.html">Patents</a></h2>
<p class="projectDescription">
	VideoLAN is seriously threatened by software patents due to the 
	numerous patented techniques it implements and uses. Also threatened
	are the many libraries and projects which VLC is built upon, like
    FFmpeg, and the other fellow Free And Open Source software
	multimedia players.
    <br />
    <b><a href="/press/patents.html">More</a></b>
</p>
<h2><a href="/press/eucd.html">EUCD</a></h2>
<p class="projectDescription">
    Press Release about DADVSI and its impact on VLC
    <br />
    <b><a href="/press/eucd.html">More</a></b>
</p>
<h2><a href="/press/2007-1.html">2007-1</a></h2>
<h4>2007</h4>
<p class="projectDescription">
    VLC media player to remain under GNU GPL version 2
    <br />
    <b><a href="/press/2007-1.html">More</a></b>
</p>
<h2><a href="/press/2006-2.html">2006-2</a></h2>
<h4>2006-04-01</h4>
<p class="projectDescription">
    Apple-VideoLAN partnership announced, Mac VLC to be Intel only
    <br />
    <b><a href="/press/2006-2.html">More</a></b>
</p>
<h2><a href="/press/2006-1.html">2006-1</a></h2>
<h4>2006</h4>
<p class="projectDescription">
    The VideoLAN website gets a new home
    <br />
    <b><a href="/press/2006-1.html">More</a></b>
</p>
<h2><a href="/press/2005-1.html">2005-1</a></h2>
<h4>2005</h4>
<p class="projectDescription">
    Following announcement by Cinema On the Web (COW) and DivXNetworks
    of the new COW video on demand service, the VideoLAN project team
    members were informed of a possible violation of VLC media
    player's license. The VideoLAN project, which owns copyrights to VLC,
    distributes VLC as free software exclusively under the terms of the
    GNU General Public License (GNU GPL).
    <br />
    <b><a href="/press/2005-1.html">More</a></b>
</p>

</div>

<?php footer(); ?>
