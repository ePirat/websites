<?php
   $title = "VideoLAN Dev Days 2009";
   $lang = "en";
   $menu = array( "project", "events" );
   $additional_css = array("/style/panels.css");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<h1> VideoLAN Dev Days 2009 </h1>
<h3> A Multimedia Conference that frees the cone in you! </h3>
<div id="left">

<h2> Welcome </h2>
<p>The <a href="/videolan/">VideoLAN non-profit organisation</a> is happy to
invite you to the multimedia open-source event of this end of year: <br />
<strong>VideoLAN Dev Days '09</strong>.</p>
<p>For the second time, people from the VideoLAN community will meet in <strong>Paris</strong> to gather, work, discuss and build a stronger community, on the <strong>18th, 19th and 20th of december 2009</strong>.</p>
<p>Developers, designers and anonymous people around <a href="/vlc/">VLC</a>, <a href="/projects/dvblast.html">DVBlast</a>, VLMa, skin-designer or other multimedia projects will be there.</p>
<p>There are <a href="#public">public</a> and <a href="#private">technical</a> events.<br />
<a href="/videolan/events/vdd09.pdf"><strong>Full schedule (updated)</strong></a>.</p>

<h1><a name="public">Public and OPEN events</a></h1>

<h2>Soirée à la Cantine</h2>
<p>On <strong>Friday 18th december 2009 at 18h30</strong>, the VideoLAN project
invites you to come to <a href="http://lacantine.org">La Cantine</a> and discuss with the VideoLAN teams.<br />
Presentations about VideoLAN, the next version of VLC and new projects will be held.</p>
<h3>Registration</h3>
<p><a href="http://lacantine.org/events/videolan-party"><strong>Registration</strong></a> are <a href="http://lacantine.org/events/videolan-party">online</a> on the <a href="http://lacantine.org/events/videolan-party">la cantine</a> website.</p>
<h3>Venue</h3>
<iframe width="520" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=la+cantine,&amp;sll=48.869775,2.342577&amp;sspn=0.006133,0.016512&amp;ie=UTF8&amp;radius=0.38&amp;rq=1&amp;ev=zi&amp;hq=la+cantine,&amp;hnear=&amp;ll=48.869775,2.342577&amp;spn=0.006133,0.016512&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=la+cantine,&amp;sll=48.869775,2.342577&amp;sspn=0.006133,0.016512&amp;ie=UTF8&amp;radius=0.38&amp;rq=1&amp;ev=zi&amp;hq=la+cantine,&amp;hnear=&amp;ll=48.869775,2.342577&amp;spn=0.006133,0.016512" style="color:#0000FF;text-align:left">View Larger Map</a></small>

<h2>Saturday Open Afternoon and dinner</h2>
<p>For the French libre community and activists or technology-oriented people that are interested, there will be technical presentations about VLC on the <strong>Saturday 19th december 2009 at 17h30</strong> at Epitech.</p>
<p>We will then go and eat together in a restaurant.</p>

<h1><a name="private">Technical events and discussions</a></h1>
<h2>What?</h2>
<p>The VideoLAN community will meet during 3 days, in order to develop the community around the cone.<br />We want to build a better future of <a href="/vlc/">VLC</a> and other projects.</p>

<h2>Who can come? </h3>
<p><strong>YOU</strong>.</p>
<p><strong>Anyone</strong> that cares about some VideoLAN projects.<br /> If you are not a technical person, but an enthusiast, we recommend to come to the less technical events, especially at La Cantine.</p>

<h2>When? </h2>
<p>On the <strong>18th, 19th and 20th of december</strong> in <strong>Paris</strong>.</p>

<h2>Where? </h2>
Technical events will be at <a href="http://epitech.eu/">Epitech</a>.

<h2>Schedule</h2>
You can find the full final schedule <a href="/videolan/events/vdd09.pdf">here</a>.

<h2>Registration for technical events</h2>
<p>If you want to come to join us, please mail us for your registration.</p>
<p>Note that the saturday conferences don't need registration.</p>
<p><a href="http://events.linkedin.com/VideoLAN-Dev-Days/pub/120441">LinkedIn Event</a>.</p>
<p><a href="http://www.facebook.com/event.php?eid=131958622787">Facebook Event</a>.</p>

<h2>Venue at Epitech</h2>
<iframe width="520" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=epitech+paris,+france&amp;sll=49.034267,2.592773&amp;sspn=3.129595,8.453979&amp;ie=UTF8&amp;hq=epitech&amp;hnear=Paris,+France&amp;ll=48.825401,2.36721&amp;spn=0.006139,0.016512&amp;z=14&amp;iwloc=A&amp;cid=7575941540416736667&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=epitech+paris,+france&amp;sll=49.034267,2.592773&amp;sspn=3.129595,8.453979&amp;ie=UTF8&amp;hq=epitech&amp;hnear=Paris,+France&amp;ll=48.825401,2.36721&amp;spn=0.006139,0.016512&amp;z=14&amp;iwloc=A&amp;cid=7575941540416736667" style="color:#0000FF;text-align:left">View Larger Map</a></small>


</div>
<div id="right">
<?php panel_start( "green" ); ?>
<h2>Sponsors</h2>
<p><a href="http://anevia.com/"><img src="/images/partners/anevia.jpg" alt="AneVIA" width=200 /></a><br /> <a href="http://anevia.com/">Anevia</a> is sponsoring the party at La Cantine.</p>
<p><a href="http://epitech.eu/"><img src="http://www.epitech.eu/images/design/logo-epitech.jpg" alt="epitech"/></a><br /> <a href="http://epitech.eu/">Epitech</a> is hosting most of the events and sponsoring some part of the event.</p>
<br />
<h2>Help us!</h2>
<p><a href="/videolan/team">Your name here!</a></p>
<p>Help us organizing the event!</p>
<?php panel_end(); ?>

<?php panel_start( "orange" ); ?>
<h2>Program</h2>
<a href="/videolan/events/vdd09.pdf"><strong>Full schedule</strong></a>.</p>

<?php panel_end(); ?>

<?php panel_start( "blue" ); ?>
<h2>VideoLAN Dev Days '08</h2>
<p class="center">
         <img src="http://download.videolan.org/events/20081220/VDD/mq/img-11.jpg" alt="VDD 08" style="width:100%"/>
</p>
<?php panel_end(); ?>

<?php panel_start( "blue" ); ?>
<h2>VLC</h2>
<p class="center">
         <img src="http://images.videolan.org/images/screenshots/vlc-win32.png" alt="VLC on Windows" />
</p>
<?php panel_end(); ?>

</div>

<?php footer('$Id: index.php 5400 2009-07-19 15:37:21Z jb $'); ?>
