<?php

$SPONSORS = array(
    array (
        "imgSrc"    => "booxmedia.png",
        "link"      => "http://www.booxmedia.com/"
    ),
    array (
        "imgSrc"    => "PSPDFKit.png",
        "link"      => "https://www.pspdfkit.com/"
    )
);

function getSponsors() {
    global $SPONSORS;
    return $SPONSORS;
}
?>
